# NBP exchange rate from CSV

### Application for getting currency exchange rate from NBP using CSV files

### Instruction:
1. First you have to get the CSV file from first link. (getExcelFile.html)
2. Then you can download it from second link. (showFiles.html)
3. Conversion between CSV and xlsx formats have done automatically.