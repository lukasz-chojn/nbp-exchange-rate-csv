package nbp_exchangerate_csv.constants;

public class Constants {
    public static final String URL_FILE_EXTENSION = ".csv";
    public static final String EXCEL_FILE_EXTENSION = ".xlsx";
    public static final String URL_FILE_PREFIX = "/archiwum_tab_a_";
    public static final String LINE_DELIMITER = ";";
    public static final String DOWNLOAD_DIRECTORY = "src/main/resources/files/csv/";
}
