package nbp_exchangerate_csv.processor;

import nbp_exchangerate_csv.constants.Constants;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;


public class FileProcessor {

    private FileProcessor() {
    }

    public static FileOutputStream processFile(String exchangeRateYear) throws IOException {

        Path pathToCSV = Paths.get("src", "main", "resources", "files", "csv");
        Path pathToExcel = Paths.get("src", "main", "resources", "files", "excel");
        File pathToFile = new File(pathToCSV.toString() + File.separator + exchangeRateYear +
                Constants.URL_FILE_EXTENSION);

        try (
                BufferedReader br = new BufferedReader(new FileReader(pathToFile, Charset.forName("cp1250")));
                Workbook workbook = new XSSFWorkbook();
                FileOutputStream out = new FileOutputStream(pathToExcel.toString() + File.separator + exchangeRateYear + ".xlsx")
        ) {
            CellStyle cellStyle = workbook.createCellStyle();
            DataFormat wbDataFormat = workbook.createDataFormat();
            cellStyle.setAlignment(HorizontalAlignment.CENTER);
            cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            cellStyle.setDataFormat(wbDataFormat.getFormat("@"));

            Sheet sheet = workbook.createSheet();
            String line = br.readLine();
            for (int rows = 0; line != null; rows++) {
                Row row = sheet.createRow(rows);
                String[] items = line.split(Constants.LINE_DELIMITER);
                for (int i = 0, col = 0; i < items.length; i++, col++) {
                    String item = items[i];
                    Cell cell = row.createCell(col);
                    cell.setCellValue(item);
                    sheet.setDefaultColumnStyle(i, cellStyle);
                }
                line = br.readLine();
            }
            workbook.write(out);
            return out;
        }
    }
}
