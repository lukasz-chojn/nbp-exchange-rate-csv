package nbp_exchangerate_csv.controller;

import nbp_exchangerate_csv.constants.Constants;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
public class ShowDataController {

    @GetMapping("/show-files")
    public ResponseEntity<String> showFile(@RequestParam("exchangeRateYear") String exchangeRateYear) {

        Path pathToExcel = Paths.get("src", "main", "resources", "files", "excel");
        File excelFile = new File(pathToExcel.toString() + File.separator + exchangeRateYear + Constants.EXCEL_FILE_EXTENSION);

        if (exchangeRateYear.isEmpty()) {
            return new ResponseEntity<>("No value of exchangeRateYear parameter", HttpStatus.BAD_REQUEST);
        }

        if (!excelFile.exists()) {
            return new ResponseEntity<>("File not found", HttpStatus.NOT_FOUND);
        }

        UriComponents uriComponents = UriComponentsBuilder
                .newInstance()
                .scheme("http")
                .host("localhost")
                .port(8080)
                .path("/file/{exchangeRateYear}")
                .buildAndExpand(exchangeRateYear + Constants.EXCEL_FILE_EXTENSION);

        return ResponseEntity
                .status(HttpStatus.OK)
                .headers(httpHeaders())
                .body(uriComponents.toUriString());
    }

    private HttpHeaders httpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.valueOf(MediaType.TEXT_PLAIN_VALUE));
        return httpHeaders;
    }
}
