package nbp_exchangerate_csv.controller;

import nbp_exchangerate_csv.constants.Constants;
import nbp_exchangerate_csv.processor.FileProcessor;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.net.URL;

@RestController
public class GetExcelFileController {

    @Value("${url}")
    private String url;

    @GetMapping("/get-file/{currencyYear}")
    public ResponseEntity<String> getExcelFile(@PathVariable String currencyYear) throws IOException {

        if (currencyYear.isEmpty()) {
            return new ResponseEntity<>("No value of currencyYear parameter", HttpStatus.BAD_REQUEST);
        }

        FileUtils.copyURLToFile(
                new URL(url + Constants.URL_FILE_PREFIX + currencyYear + Constants.URL_FILE_EXTENSION),
                new File(Constants.DOWNLOAD_DIRECTORY + currencyYear + Constants.URL_FILE_EXTENSION));

        FileProcessor.processFile(currencyYear);

        return new ResponseEntity<>("File correctly downloaded", HttpStatus.OK);
    }
}
